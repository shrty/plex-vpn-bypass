# Plex VPN Bypass

Read forum thread: https://forums.plex.tv/discussion/309015/nas-synology-vpn-bypass-plex

## How to use

Deploy this script to your preferred directory on your Synology NAS

1. ```chmod +x plex.sh```
2. ```sh plex.sh```

You'll get prompted with sudo password after supplying gateway and interface.
Confirm that traffic goes through correct interface

3. ```traceroute -n -m 1 plex.tv```

Please note that you need to rerun this script after reboot.
This can easily be fixed by setting up a startup script