<?php

/**
 * Simple PHP script to generate routes for bypassing active VPN Connection
 * OP https://forums.plex.tv/discussion/309015/nas-synology-vpn-bypass-plex
 *
 * Usage: php plex.php
 *
 * @author Stephen Fenne <sfenne@gmail.com>
 */

class Plex {

    protected $cmd;
    protected $hostname = 'plex.tv';
    protected $gw = '192.168.1.1';
    protected $iFace = 'ovs_eth1';
    protected $routeFile = 'routes.sh';

    public function __construct() {
        
        $this->askQuestion('Type your gateway ['.$this->gw.']', 'gw');
        $this->askQuestion('Type your interface ['.$this->iFace.']', 'iFace');

        if($result = dns_get_record($this->hostname, DNS_A)) {

            foreach ($result as $record) {
                $ip = long2ip(ip2long($record["ip"]) & 0xFFFFFF00);
                $this->cmd .= "ip route add {$ip}/24 via {$this->gw} dev " . $this->iFace . PHP_EOL;
            }

            $this->writeln("Success. Found ".sizeof($result)." hosts. Showing routes...");
            $this->writeln("-------------------------------");
            $this->writeln($this->cmd);
            $this->askQuestion("Looks good? [y/N]", null, true);

            $this->writeln("Applying routes...");
            sleep(1);
            file_put_contents($this->routeFile, $this->cmd);
            exec("sudo sh ".$this->routeFile);
        }

    }

    private function askQuestion($q, $var=null, $requiredInput=null) {

        $this->writeln($q, false);

        flush();

        if($requiredInput) {
            $confirmation  =  trim(fgets(STDIN));
            if ( strtolower($confirmation) !== 'y' ) {
                echo ">> Cancelled".PHP_EOL;
                exit;
            }
        }

        if($var) {
            $this->$var = trim(fgets(STDIN)) ? : $this->$var;
        }
    }

    private function writeln($message,$newline = true) {
        print '>> '.$message.($newline ? PHP_EOL : ' ');
    }
}

// initiate script
$plex = new Plex();